#!/bin/bash

# Check if the prefix is given
if [ "$#" -ne 1 ]; then
    echo "Invalid call. Use fix_evr_local_time.sh EVRPREFIX"
    exit 1
fi

prefix=$1

# Loop until the EVR time is valid
while [ "$(caget -t "${prefix}:Time-Valid-Sts")" != "Valid" ]; do
    echo "Changing frequency..."
    caput "${prefix}:Link-Clk-SP" 150
    sleep 15
    caput "${prefix}:Link-Clk-SP" 88.0525
    sleep 15
done

echo "Now the EVR has a valid time!"

